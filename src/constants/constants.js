export const API = 'https://api.flickr.com/services/rest/?method=';
export const WEB = 'https://www.flickr.com/photos/';
export const ALBUMS = 'flickr.photosets.getList';
export const IMAGES = 'flickr.photosets.getPhotos';
export const COMMENTS = 'flickr.photos.comments.getList';
export const KEY = 'f781834301f12153533beaa447c6d72b';
export const USER = '188650951@N03';
